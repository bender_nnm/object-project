/*  opts:
 * saveMeta: boolean           - project undefined meta value too
 * saveNull: boolean           - project null value too
 * saveUndefined: boolean      - project undefined value too
 * silence: boolean            - don't call function
 * json: boolean               - parse result to boolean
 * mergeTo: object             - use not empty object, set result to this object
 * meta: object                - some important value, those need add to object
 */

function parseObject(object, project, result, opts) {
    var field;
    var objectValue;
    var projectValue;

    for (field in project) {
        if (!project.hasOwnProperty(field)) {
            continue;
        }

        objectValue = object[field];
        projectValue = project[field];

        if (projectValue
            && (opts.saveNull || objectValue !== null)
            && (opts.saveUndefined || objectValue !== undefined)) {

            switch (typeof projectValue) {
                case 'number':
                    projectValue && (result[field] = objectValue);
                    break;
                case 'string':
                    opts.saveMeta || opts.meta[projectValue] && (result[field] = opts.meta[projectValue]);
                    break;
                case 'function':
                    !opts.silence && (result[field] = projectValue(objectValue));
                    break;
                case 'object':
                    parseObject(objectValue, projectValue, (result[field] = {}), opts);
                    break;
                default:
                    break;
            }

        }
    }

    return result;
}

module.exports = function (object, project, opts) {
    var json;
    var result;
    var mergeTo;

    opts || (opts = {});
    opts.meta || (opts.meta = {});

    (json = opts.json) && delete opts.json;
    (mergeTo = opts.mergeTo) && delete opts.mergeTo;

    result = parseObject(object, project, mergeTo || {}, opts);

    return json ? JSON.stringify(result) : result;
};
