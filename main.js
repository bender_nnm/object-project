var projectObject = require('./index');

var objectTest1 = {
    date          : new Date(),
    customer      : {
        person: {
            fullName: {
                name   : 'Dmytro',
                surname: 'Olashyn'
            },
            age     : 2
        }
    },
    left          : 1,
    right         : 1,
    cash          : {
        usa : {
            type : {
                credit: true,
                cash  : true
            },
            value: 200
        },
        uah : {
            type : {
                credit: false,
                cash  : true
            },
            value: 100
        },
        euro: {
            type : {
                credit: true,
                cash  : false
            },
            value: 300
        }
    },
    additionalData: {
        first : [2, 3, 4],
        second: [2, 3, 4]
    }
};
var projectFields1 = {
    date          : 1,
    left          : 1,
    right         : '$meta_1',
    main          : 1,
    cash          : {
        usa : 1,
        euro: {
            value: 1
        },
        uah : {
            type: {
                credit: 1
            }
        }
    },
    additionalData: {
        first : {
            '1': 1
        },
        second: 1
    },
    customer      : {
        person: {
            fullName: {
                name   : 1,
                surname: 1
            }
        }
    }
};
var opts1 = {
    meta: {
        '$meta_1': 'META'
    }
};

var objectTest2 = {
    a: 'A',
    b: {
        c1: 'C1',
        c2: {
            c4: {
                value: 100
            },
            c6: {
                c7: 'qwerty',
                c8: 8
            }
        }
    },
    c: null
};
var projectFields2 = {
    a: 1,
    b: {
        c1: 1,
        c2: {
            c4: 1,
            c5: 1,
            c6: {
                c7: function (item) {
                    return item.toUpperCase();
                },
                c8: 1
            }
        },
        c3: 1
    },
    c: 1
};
var opts2 = {
    silence      : true,
    saveNull     : true,
    saveUndefined: true
};

var result1 = projectObject(objectTest1, projectFields1, opts1);
var result2 = projectObject(objectTest2, projectFields2, opts2);

console.log(result1);
console.log(result2);
